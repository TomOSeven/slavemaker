SlaveMaker 3
============

Intro
-----
This is a fan hentai parody of the game 'Princess Maker'. You are a slave trainer in the kingdom of Moiya. You are contracted to train a girl to become the ideal slave. She will be trained in many fields but sex is a significant part of the training. Each girl has a training time before she is turned over to her owner and in that time you need to try have maximise her abilities.

There are dresses and other items that help this, and also potions of various effects. Part of the interest in the game can be learing what these things do and where they can be found.

The girls are various characters from anime and games, their personalities will be similar but not identical to their source. Also note for this game they are all 18 or older (despite the occiasonal school uniform you may see).

You require an assistant for your training so you are required to train Shampoo first (this can be bypassed in options if desired)

Some more exterem hentai elements are in this game, including bondage, rape, ponygirls, tentacle sex and dickgirls. The last three can be disabled in options on the slave selection screens.

Also note this is fan or doujin game, so is not for sale in any way. My appologies to the original copyright holders and artists. No offense is intended.

Usage
-----
Windows: run SlaveMaker3.exe or run in you browser SlaveMaker3.swf
Others: run SlaveMaker3.swf


Notes on this version
--------------------
This is my revision of the flash game 'SlaveMaker' released some time ago. My apologies to the original author there was no way to contact him to get approval for this version. I very much enjoyed it but found some bugs and limitations in it, so I decided to learn flash programming and fix it.

I have removed some annoying bugs in the original and expanded and gramar corrected language usage. The original author appears to be French (all the code was in french). Graphics tweaked and expanded and resolution increased.

I have also added additional game play elements, notably ponygirls, tentacle sex and dickgirls. If these are not your thing they can be disabled in options.

I have made training Shampoo mandatory to start with.

I have removed Skuld from the game (she was in the original version), but I feel she is too young. I have added Urd, Tifa, Kasumi, Ayane and have made it easy to add more girls. I have several more in developement.

I have done a lot of tweaks and changes to game play, including
1) added ability to save up to 5 games
2) shortcut keys, try first letter of a button, or the letter in blue. Some buttons do not have the shortcuts though. On the main screen 'S' saves to save game 1 and 'L' loads save game 1
3) difficulty levels (see options)
4) dickgirl encounters and the ability of some girls to transform into a dickgirl
5) encounters with tentacle monsters
6) many, many more graphics, an average of 3 alternate images, and up to 20
7) new items
8) more places, people to meet


Special Events/Ending
----------------------
Akane - standard ends, shampoo trouble event, noble love event
Ayane - Bondage start, Loyal end
Belldandy - Demon or Angel or Mortal ends, Nun seduction event
Rama - Dickgirl or Catgirl ends, catgirl effects, Running Away End, Lesbian Lovers end
Tifa - Dickgirl, tetacle magnet
Urd - Alchemist or Dickgirl ends, potion shop
Naru - Nudist, Cosplay, Bride of the Goddess
Yurika - Dickgirl Milking and/or Dickgirl Cow, Idol End
Shampoo - standard, noble love, Perfect Slave End, Dickgirl Mother end
Kasumi - Winner end, Snacks event, Cumslut end
Riesz - Tentacle Queen, Wolf Knight, Dickgirl Toy endings, events demon morning
Minako - Cat Slave, Cat Saviour endings, events - morning, autographs, true catgirl rescue, catgirl meetings
Orihime - Drug Whore End, Corrupt Priestess End, Demon Lover End, Demoness End
Menace - Failed Slave End, Drug Whore End, Slave Queen End, Arena Champion End, Exile Queen End, Victorious QUeen End
Rei - Demon Lover End, Failed Exorcist/Failed Exorcist Corrupt End,Pure End, Assistant End, Bride of the Gods End, Dickgirl alternate endings for all special endings

All
Tentacle Harem end
Tentacle Abduction event


Gameplay Tips
-------------
slightly obscure ones

Three is the charm
The ultimte dress is needed for some ends
tentacles like the dark
Normal difficulty is original



see lower for more detailed
















































Gameplay Tips
-------------
Detailed ones


1) Tifa, Ranma and Urd can permanently transform into a dickgirl. Drink Astrid's potion 3 times. Each have paths to be cured but should be obvious

2) Belldandy
Demon: - buy and wear the Angelic Dress, get libido, nymphomania 80+, visit docks and will be offered transformation, accept
Angel: - as above but refuse, then visit lake, accept offer
Mortal - as above and refuse both, or dont do path (ie dont wear dress)

3) Urd
when she drinks a potion 3 times she can then make it. When she can make 3 potions then she will offer to open a potion shop

4) Yurika - drink Astrids potion 3 times and she wil start transforming spontaneously. Some time later she will run away...

5) Ayane - get her Rebellion - (obedience + morality/2) below 10

5) Ponygirl - get her to do a bondage action, and visit docks a few times, will be given a bitgag. Visit Palace a few times to get a ponytail. Make sure money > 500 and you will travelling salesman will offer a harness, buy it. Do at least 5 bondage actions. Once her obedience reaches a high enough level she will become your ponygirl. It is very difficult to win the ponygirl race on highest difficulty factor. Need high constitution, low fatigue

6) score is the average of stats - fatigue + hidden love stat. Accepting love confession reduces by 20

7) Minako - wear the catgirl dress! - ask about life as catgirl and true cat girls, work in brothel/sleazy bar until you get a knowledgable customer/fellow worker. She is font of knowledge about true cat girls. visit lake and start quest

8) Evil Mine
Visit the Slave Pens until you have an encounter with a slave, Sareth
visit the Secure Slave Pens until you see an escape, and try to rescure the woman, Lady Farun
If you can visit the ruins go there
otherwise visit Lady Farun. She will give you access after a few days, and a request for a potion
Potion needs to be located (Odd Teacher supplies it but you need to find that out)
Zombies are tough, Akanes mallet is the best weapon
Guaranteed win if you hire Misana at seers (1000GP), Bounty hunter good, lake water good, others minor effect

9) Rei
Pure - win most events and follow the gods
Corrupt - lose, submit, question the gods
Assistant/Experienced - seek fun and variety
Chinese - dress accompany Miss N

10) Menace
Slave Queen - Win the first fight against Anistaria, then lose the second fight in the ruins
Areana Champion - Defeat the Masked Champion in the Arena (aquire Setra and find two keys)
Exile Queen - Defeat Anistaria in the Ruins behind the gold key door, recruit skeleton royal gaurd, recruit Melona, Seduce Nobles
Victorious Queen - Defeat Anistaria in the ruins behind the gold key door after getting Arena Champion Ending

11) Orihime
Priestess - Find Matsumoto and have her lift the curse, then agree to serve as a Priestess of the Old Gods, and have a morality of 85 or over
Corrupt Priestess - As above but sleep with the congregation and have a morality of or lower than 25
Demon Lover - Wear the Demon's Dress, Sleep with the darkness until your given a choice to accept it or not and say yes, sleep with shadow and then go to the slum and buy the tail, sleep with the shadow again then on the night of a full moon go to the farm and get the horns, don't tie Orihime up during the eclipse and and exactly at noon go to the lake, say no to letter her inner demon out
Demoness - As above but agree to let inner demon out
Drug Whore - Take nymphs tears three times or more